/// <reference types="react-scripts" />

interface IVote {
  candidateName: string;
  totalVotes: number;
}

interface ElectionResults {
  winner: string;
  votes: {
    [key: string]: number;
  };
}
