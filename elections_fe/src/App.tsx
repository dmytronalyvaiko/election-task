import React from 'react';
import { observer } from 'mobx-react';

import { ElectionStore } from 'stores/ElectionStore';
import { Election } from 'components/Election';

import 'App.css';

interface IAppProps {
  electionStore: ElectionStore
}

@observer
class App extends React.Component<IAppProps> {
  render() {
    const { electionStore } = this.props;

    return (
      <div className='App'>
        <Election
          findWinner={electionStore.getElectionResults}
          winner={electionStore.winner}
          votes={electionStore.votes}
          error={electionStore.error}
        />
      </div>
    )
  }
}

export default App;
