import { action, observable } from "mobx";

import { api } from "api/api";

export class ElectionStore {
  @observable winner?: string;
  @observable votes: IVote[] = [];
  @observable error?: string;

  @action
  getElectionResults = async () => {
    let result;

    try {
      result = await api.getElectionResults();
    } catch (err) {
      this.error = "Couldn't reach the server. Please contact out support.";
    }

    if (!result) {
      return;
    }

    if (this.error) {
      this.error = undefined;
    }

    this.winner = result.winner;
    this.votes = Object.entries(
      result.votes
    ).map(([candidateName, totalVotes]) => ({ candidateName, totalVotes }));
  };
}
