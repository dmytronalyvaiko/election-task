import React from "react";

interface ICandidateResult {
  name: string;
  totalVotes: number;
}

export const CandidateResult = (props: ICandidateResult) => {
  return (
    <div>
      - {props.name}:{props.totalVotes}
    </div>
  );
};
