import React from "react";
// import { IVote } from "common/types";
import { CandidateResult } from "./CandidateResult";

interface IVotes {
  votes: IVote[];
}

const getCandidatesResults = (votes: IVote[]) =>
  votes.map((candidateResult) => (
    <CandidateResult
      key={candidateResult.candidateName}
      name={candidateResult.candidateName}
      totalVotes={candidateResult.totalVotes}
    />
  ));

export const Votes = (props: IVotes) => {
  const { votes } = props;

  return (
    <div>
      <>
        <h3>List of votes:</h3>
        {getCandidatesResults(votes)}
      </>
    </div>
  );
};
