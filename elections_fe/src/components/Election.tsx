import React from "react";

import { Votes } from "./Votes";
import { Winner } from "./Winner";

interface IElection {
  findWinner: () => void;
  winner?: string;
  votes: IVote[];
  error?: string;
}

const getVotes = (votes: IVote[]) =>
  votes.length ? <Votes votes={votes} /> : null;

const getWinner = (winner?: string) =>
  winner ? <Winner winner={winner} /> : null;

const getContent = (votes: IVote[], error?: string, winner?: string) =>
  !error ? (
    <>
      {getVotes(votes)}
      <br />
      {getWinner(winner)}
    </>
  ) : (
    <span className='error'>{error}</span>
  );

export const Election = (props: IElection) => {
  const { votes, winner, error, findWinner } = props;

  return (
    <>
      <button onClick={findWinner}>
        <h4>Find Winner</h4>
      </button>
      {getContent(votes, error, winner)}
    </>
  );
};
