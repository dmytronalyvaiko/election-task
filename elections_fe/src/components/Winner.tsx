import React from "react";

interface IWinner {
  winner?: string;
}

export const Winner = (props: IWinner) => {
  const { winner } = props;

  return <div>The winner is <mark>{winner}</mark>!</div>;
};
