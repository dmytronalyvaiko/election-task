import { API_URI, API_ELECTION_RESULTS_PATH } from "config";

const getElectionResults = async (): Promise<ElectionResults> => {
  const fetchResult = await fetch(`${API_URI}/${API_ELECTION_RESULTS_PATH}`);

  return fetchResult.json();
};

export const api = {
  getElectionResults,
};
