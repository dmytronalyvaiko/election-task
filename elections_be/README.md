# elections_be

The simple http server with one supported endpoint, `/election_results`, which returns the calculated winner along with the election results of randomly generated elections every time a request is made.

The default listening port is `3001` if the environment variable `APP_PORT` is not specified.

### How to use it

Install:
 - `yarn install` or `npm install`

Run in watch(development) mode:
 - `yarn start:dev` or `npm run start:dev`

Run in deployment environment(test/stage/prod):
  - `yarn start` or `npm start`

Unit tests:
  - `yarn test` or `npm test`

### Notes
Since some candidates may have the same number of votes, only one of them will be selected as the winner (the first one found in the internally generated array) in accordance with the current algorithm for determining the winner. The function that implements the algorithm is located in `services/utils -> findWinner`
