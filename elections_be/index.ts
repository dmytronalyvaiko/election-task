import * as express from "express";
import { routes } from "./api/routes";

export const bootstrap = () => {
  const app = express();

  routes(app);

  return app;
};
