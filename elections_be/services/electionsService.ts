import { database } from "./database";
import { findWinner, IWinner } from "./utils";

export default {
  async getResults(): Promise<IWinner> {
    const allVotes = await database.findAllVotes();
    const winner = findWinner(allVotes);

    return winner;
  },
};
