import * as faker from "faker";

const mockDB = {
  generateCandidates() {
    const totalCandidates = faker.random.number({ min: 4, max: 5 });

    return Array.from(Array(totalCandidates)).map(
      () => `${faker.name.firstName()} ${faker.name.lastName()}`
    );
  },
  generateVotes(candidates: string[]) {
    const totalVotes = faker.random.number({ min: 7, max: 15 });

    return Array.from(Array(totalVotes)).map(
      () =>
        candidates[faker.random.number({ min: 0, max: candidates.length - 1 })]
    );
  },
  async findAllVotes(): Promise<string[]> {
    const candidates = this.generateCandidates();

    return this.generateVotes(candidates);
  },
};

export const database = mockDB;
