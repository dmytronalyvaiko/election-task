interface IVotes {
  [key: string]: number;
}

export interface IWinner {
  winner: string;
  votes: IVotes;
}

export const findWinner = (votes: string[]): IWinner => {
  if (!votes.length) {
    return null;
  }

  const winner = {
    votesAmount: 1,
    name: votes[0],
  };

  const candidateVotesAmount = {};

  for (const vote of votes) {
    candidateVotesAmount[vote] = (candidateVotesAmount[vote] || 0) + 1;

    if (candidateVotesAmount[vote] > winner.votesAmount) {
      winner.votesAmount = candidateVotesAmount[vote];
      winner.name = vote;
    }
  }

  return {
    winner: winner.name,
    votes: candidateVotesAmount,
  };
};
