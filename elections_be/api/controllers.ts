import { Response, NextFunction } from "express";

import electionsService from "../services/electionsService";
import { ServiceError } from "../helpers/ServiceError";

export const getElectionResults = async (
  req,
  res: Response,
  next: NextFunction
) => {
  try {
    const results = await electionsService.getResults();

    res.status(200).json(results);
  } catch (err) {
    next(
      new ServiceError(
        500,
        "Something went wrong... Please, contact our support for help."
      )
    );
  }
};
