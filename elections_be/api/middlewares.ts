import { Response } from "express";

import { ServiceError } from "../helpers/ServiceError";

export const handleErrorMiddleware = (
  err: ServiceError,
  _req,
  res: Response,
  _next
) => {
  const { statusCode, message } = err;

  res.status(statusCode).json({
    status: "error",
    statusCode,
    message,
  });
};
