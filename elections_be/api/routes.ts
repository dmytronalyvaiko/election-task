import * as cors from "cors";
import { getElectionResults } from "./controllers";
import { handleErrorMiddleware } from "./middlewares";

export const routes = (app) => {
  app.use(cors());

  app.route("/election_results").get(getElectionResults);

  app.use(handleErrorMiddleware);
};
