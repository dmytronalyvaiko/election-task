import { findWinner } from "../../services/utils";

describe("electionsService -> findWinner", () => {
  it("should return null when passed empty array", () => {
    expect(findWinner([])).toEqual(null);
  });

  it("should return the winner when passed an array with 1 value", () => {
    expect(findWinner(["Adam Adams"])).toEqual({
      winner: "Adam Adams",
      votes: {
        "Adam Adams": 1,
      },
    });
  });

  it("should return the winner when passed an array with 2 similar values", () => {
    expect(findWinner(["Adam Adams", "Adam Adams"])).toEqual({
      winner: "Adam Adams",
      votes: {
        "Adam Adams": 2,
      },
    });
  });

  it("should return 1st candidate when passed an array with 2 different values", () => {
    expect(findWinner(["Adam Adams", "Bob Bobson"])).toEqual({
      winner: "Adam Adams",
      votes: {
        "Adam Adams": 1,
        "Bob Bobson": 1,
      },
    });
  });

  it("should return the most voted candidate when passed an array with 3 values", () => {
    expect(findWinner(["Adam Adams", "Bob Bobson", "Bob Bobson"])).toEqual({
      winner: "Bob Bobson",
      votes: {
        "Adam Adams": 1,
        "Bob Bobson": 2,
      },
    });

    expect(findWinner(["Bob Bobson", "Adam Adams", "Bob Bobson"])).toEqual({
      winner: "Bob Bobson",
      votes: {
        "Adam Adams": 1,
        "Bob Bobson": 2,
      },
    });

    expect(findWinner(["Bob Bobson", "Bob Bobson", "Adam Adams"])).toEqual({
      winner: "Bob Bobson",
      votes: {
        "Adam Adams": 1,
        "Bob Bobson": 2,
      },
    });
  });

  it("should return the most voted candidate when passed an array with 7 values", () => {
    expect(findWinner(["A", "B", "A", "C", "D", "B", "A"])).toEqual({
      winner: "A",
      votes: {
        A: 3,
        B: 2,
        C: 1,
        D: 1,
      },
    });
  });
});
