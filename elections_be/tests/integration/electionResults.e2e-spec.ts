import * as request from "supertest";

import { bootstrap } from "../../index";

const app = bootstrap();

describe("GET /election_results", () => {
  let response: request.Response;

  beforeAll(async () => {
    response = await request(app).get("/election_results");
  });

  it("should respond with status code 200", () => {
    expect(response.status).toBe(200);
  });

  it("should respond with content-type of application/json", () => {
    expect(response.header["content-type"]).toMatch("application/json");
  });

  it("should respond with object with proper fields", () => {
    expect(response.body).toEqual({
      winner: expect.any(String),
      votes: expect.any(Object),
    });
  });
});
