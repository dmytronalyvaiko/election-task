import { bootstrap } from ".";

const APP_PORT = process.env.APP_PORT || 3001;

bootstrap().listen(APP_PORT, () =>
  console.log(`Server started on APP_PORT: ${APP_PORT}`)
);
