export class ServiceError extends Error {
  constructor(public statusCode: number, public message: string) {
    super();
  }
}
